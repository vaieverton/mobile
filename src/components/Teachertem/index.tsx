import React from 'react';
import { Image, Text, View } from 'react-native';
import { RectButton } from 'react-native-gesture-handler';

import heartOulineIcon from '../../assets/images/icons/heart-outline.png';
import unfavoriteIcon from '../../assets/images/icons/unfavorite.png';
import whatsappIcon from '../../assets/images/icons/whatsapp.png';

import styles from './styles';

const Teachertem: React.FC = () => {
  return (
    <View style={styles.container}>
        <View style={styles.profile}>
            <Image 
                style={styles.avatar}
                source={{uri: 'https://avatars.githubusercontent.com/u/69922518?s=460&u=759f25115b93ba66dc6ca0573680dcca14f05dd6&v=4'}}
            />

            <View style={ styles.profileInfo }>
                <Text style={ styles.name } >Everton de Souza</Text>
                <Text style={ styles.subject } >Química</Text>
            </View>
        </View>

        <Text style={styles.bio}>Aasdfkljakljfkla alsjflasf lasjdlkjfasdjal laksjdfklajsdflajs</Text>

        <View style={styles.footer}>
            <Text style={styles.price}>
                Preço/hora {' '}  
                <Text style={styles.priceValue}>R$ 20,00</Text>
            </Text>

            <View style={styles.buttonsContainer}>
                <RectButton style={[styles.favoriteButton, styles.favorited] } > 
                    {/* <Image source={heartOulineIcon} /> */}
                    <Image source={unfavoriteIcon} />

                </RectButton>

                <RectButton style={styles.contactButton } >
                    <Image source={whatsappIcon} />
                    <Text style={styles.contactButtonText}>Entrar em contato</Text>
                    
                </RectButton>
            </View>
        </View>
    </View> 
  )
}

export default Teachertem;