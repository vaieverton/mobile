import React from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Landing from '../pages/Landing';
import GiveClasses from '../pages/GiveClasses';
import StudyTabs from '../routes/StudyTabs';
import Listagem from '../pages/Listagem';

const { Navigator, Screen } = createStackNavigator();

function AppStack () {
    return (
        <NavigationContainer>
            <Navigator screenOptions={{ headerShown: false }}>
                <Screen name="Listagem" component={Listagem} />
                <Screen name="Landing" component={Landing} />
                <Screen name="GiveClasses" component={ GiveClasses } />
                <Screen name="Study" component={ StudyTabs } />
            </Navigator>
        </NavigationContainer>
    )
}

export default AppStack;