import React from 'react';
import { Image, View, Text, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native'

import logoImg from '../../assets/images/landing.png';
import studyIcon from '../../assets/images/icons/study.png';
import giveClassesIcon from '../../assets/images/icons/give-classes.png'
import hearthIcon from '../../assets/images/icons/heart.png';

import styles from './styles';

function Landing() {
    const navigation = useNavigation();

    function handleGiveClassesNav () {
        navigation.navigate('GiveClasses')
    }

    function handleStudyNav () {
        navigation.navigate('Study')
    }

    return (
        <View style={styles.container}>
            <Image source = { logoImg }  style={styles.banner} />

            <Text style={styles.title}>
                Seja bem-vindo, {'\n'}
                <Text style={styles.titleBold}>O que deseja fazer?</Text>
            </Text>

            <View style={styles.buttonsContainer}>
                <TouchableOpacity onPress={handleStudyNav} style={[ styles.button, styles.buttonPrimary ]}>
                    <Image source={studyIcon} />
                    <Text style={styles.buttonText}>Estudar</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={handleGiveClassesNav} style={[ styles.button, styles.buttonSecondary ]}>
                    <Image source={giveClassesIcon} />
                    <Text style={styles.buttonText}>Dar aulas</Text>
                </TouchableOpacity>
            </View>


            <Text style={styles.totalConnections}>
                Total de 285 conexões realizadas 
                <Image source={hearthIcon} />
            </Text>
        </View>
    )
}

export default Landing;