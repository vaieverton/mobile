import React from 'react';
import { View, ScrollView } from 'react-native';
import PageHeader from '../../components/PageHeader';
import Teachertem from '../../components/Teachertem';

import styles from './styles';

function Favorites() {
    return(
        <View style={styles.container}>
            <PageHeader title="Meus Proffys favoritos"/>
            <ScrollView
                style={styles.teacherList}
                contentContainerStyle={{
                    paddingHorizontal: 16,
                    paddingBottom: 16
                }}
            >
                <Teachertem />
                <Teachertem />
                <Teachertem />
                <Teachertem />
                <Teachertem />
                <Teachertem />
            </ScrollView>
        </View> 
    )
}

export default Favorites;