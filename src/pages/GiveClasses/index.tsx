import React from 'react';

import giveClassesBg from '../../assets/images/give-classes-background.png';

import { View, ImageBackground, Text, TouchableOpacity } from 'react-native';
import styles from './styles';
import { useNavigation } from '@react-navigation/native';

function GiveClasses () {
    const { goBack } = useNavigation();

    return (
        <View style={styles.container}> 
            <ImageBackground 
                resizeMode="contain" 
                source={giveClassesBg} 
                style={styles.content}
            >
                <Text style={styles.title}>Quer ser um Proffy?</Text>
                <Text style={styles.description}>
                    Para começar, você precisa se cadastrar como professor na nossa
                    plataforma web
                </Text>
            </ImageBackground>
            <TouchableOpacity onPress={ goBack } style={styles.okButton}>
                <Text style={styles.okButtonText}>Tudo bem</Text>
            </TouchableOpacity>
        </View>
    )
}

export default GiveClasses;