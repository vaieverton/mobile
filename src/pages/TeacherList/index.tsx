import React from 'react';
import { ScrollView, Text, TextInput, View } from 'react-native';
import PageHeader from '../../components/PageHeader';
import Teachertem from '../../components/Teachertem';

import styles from './styles';

function TeacherList () {
    return (
        <View style={styles.container}>
            <PageHeader title="Proffy's Disponíveis">
                <View style={styles.searchForm}>
                    <Text style={styles.label} >Matéria</Text>
                    <TextInput 
                        style={styles.input}
                        placeholder="Qual a matéria?"
                        placeholderTextColor="#c1bccc"
                    />


                    <View style={styles.inputGroup}>
                        <View style={styles.inputBlock}>
                            <Text style={styles.label}>
                                Dia da semana
                            </Text>
                            <TextInput 
                                style={styles.input}
                                placeholder="Qual o dia?"
                                placeholderTextColor="#c1bccc"
                            />
                        </View>

                        <View style={styles.searchForm}>
                            <Text style={styles.label} >Matéria</Text>
                            <TextInput 
                                style={styles.input}
                                placeholder="Qual o horário?"
                                placeholderTextColor="#c1bccc"
                            />
                        </View>
                    </View>
                </View>
            </PageHeader>
            <ScrollView
                style={styles.teacherList}
                contentContainerStyle={{
                    paddingHorizontal: 16,
                    paddingBottom: 16
                }}
            >
                <Teachertem />
                <Teachertem />
                <Teachertem />
                <Teachertem />
                <Teachertem />
                <Teachertem />
            </ScrollView>
        </View> 
    );
}

export default TeacherList;