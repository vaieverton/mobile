import React, { useCallback, useEffect, useState } from 'react';
import {View, Text} from 'react-native';
import { RectButton } from 'react-native-gesture-handler';

import api from '../../services/api';

import styles from './styles';


// ABSTRAÇÃO DOS BOTÕES DE FILTRO DE NOME E DATA

const Listagem: React.FC = () => {
    const buttonNameAZtoZA = (
        <RectButton onPress={() => {
            console.log('AZ PARA ZA')
            handleFilterClick('nameZA', filterNameOrDate);
        }}>
            <Text>NAME +</Text>
        </RectButton>
    )

    const buttonNameZAtoAZ = (
        <RectButton onPress={() => {
            console.log('ZA PARA AZ')
            handleFilterClick('nameZA', filterNameOrDate);
        }}>
            <Text>NAME -</Text>
        </RectButton>
    )
        // B = BIGGER | S = SMALLER
    const buttonDateB = (
        //será exibido caso filtro == dateMaiorMenor
        <RectButton onPress={() => {
            console.log('BOTAO maior menor'+filterNameOrDate)
            handleFilterClick('dateMenorMaior', filterNameOrDate);
        }}>
            <Text>DATA +</Text>
        </RectButton>
    )

    const buttonDateS = (
        //será exibido caso filtro == dateMenorMaior
        <RectButton onPress={() => {
            console.log('BOTAO MENOR MAIOR'+filterNameOrDate)
            handleFilterClick('dateMaiorMenor', filterNameOrDate);
        }}>
            <Text>DATA -</Text>
        </RectButton>
    )

    //FAZER USEEFFECT QUE DEPENDA DE: FILTERANALISE E FILTERNAEORDATE QUE FAÇA A CHAMADA A API E LISTE
    const [filterAnalise, setFilterAnalise] = useState <string | undefined>();

    const [ filterNameOrDate, setFilterNameOrDate ] = useState ('nameAZ');

    const token = 'eyJhbGciOiJIUzUxMiJ9.eyJhdXRoIjp7InVzdWFyaW8iOnsiaWQiOiJlMmRhYWEzNC0xMDFmLTRjMDQtYTZiYS1lYmY3NzM1MmUzMTgiLCJub21lIjoiU3VwZXIgVXN1w6FyaW8iLCJlbWFpbCI6InN1cGVyQHN1cGVyLmNvbSJ9LCJlbXByZXNhIjoiQ0xJRU5URSBURVNURSIsImdydXBvcyI6W119LCJpc3MiOiJBQ0NFU1MiLCJpYXQiOjE2MTE2MjUxMzYsImV4cCI6MTYxMTcxMTUzNn0.G3RyMPTpCcbzq7k-qudl8eyddPwteaHefv4jqTsVj2G2Vl6z4t2ZCQLuSvCJ9BeGyg54DtAU96tGw4-OGfwMeg';

    useEffect(() => {
        handleApiRequest(filterNameOrDate, filterAnalise, );
    }, [filterNameOrDate, filterAnalise])

    const handleApiRequest = useCallback( async ( filtroNomeData: string, filtroAnalise?: string, filtro) => {
        if( !filtroAnalise ){
            const response = await api.get(`gerenciamento?orderBy=${filtroNomeData}`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            // console.log(response.data)
        } else {
            const response = await api.get(`gerenciamento?orderBy=${filtroNomeData}`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            // console.log(response.data)
        }
        
    }, [])

    const pressCollorButton = useCallback((value: string, currentFilter)=> {
        if (currentFilter === value) {
            setFilterAnalise(undefined);
        }
        else {
            setFilterAnalise(value)
        }
    }, []);

    //FILTROS POSSIVEIS: 

    // dateMaiorMenor
    // dateMenorMaior
    // nameAZ
    // nameZA

    const handleFilterClick = useCallback((filterToBe: string, whichFilterIs: string) => {
        if (filterToBe === whichFilterIs) {
           setFilterNameOrDate('nameAZ');
           return;
        }else {
            setFilterNameOrDate(filterToBe)
        }
    } , [])

  return (
    <View style={styles.container}>
        <View style={styles.filters}>
            <RectButton style={styles.green} onPress={() => pressCollorButton('conforme', filterAnalise)}>
                <View style={filterAnalise == 'conforme' ?
                        {borderColor: '#000', borderWidth: 2, width: 16, height: 16, borderRadius: 8} : 
                        {borderColor: '#fff'}
                    } 
                />
            </RectButton>
            <RectButton style={styles.red} onPress={() => pressCollorButton('nao_conforme', filterAnalise)}>
                <View style={filterAnalise == 'nao_conforme' 
                            ? {borderColor: '#000', borderWidth: 2, width: 16, height: 16, borderRadius: 8} : 
                            {borderColor: '#fff'} 
                        } 
                />
            </RectButton>
            <RectButton style={styles.yellow} onPress={() => pressCollorButton('uso_restrito', filterAnalise)}>
                <View style={filterAnalise == 'uso_restrito' 
                            ? {borderColor: '#000', borderWidth: 2, width: 16, height: 16, borderRadius: 8} : 
                            {borderColor: '#fff'} 
                        } 
                />
            </RectButton>
        </View>



        <Text>Estado do filtro: {filterAnalise}</Text>

        
        {filterNameOrDate == 'nameAZ' ? buttonNameZAtoAZ : buttonNameAZtoZA}
        {filterNameOrDate == 'dateMaiorMenor' ? buttonDateS : null}
        {filterNameOrDate == 'dateMenorMaior' ? buttonDateB : null}
        {filterNameOrDate != 'dateMaiorMenor' && filterNameOrDate != 'dateMaiorMenor'
                            ? buttonDateB : null}
        

        <Text>Estado do filtro data ou alfa: {filterNameOrDate}</Text>
    </View>
  )
}

export default Listagem;