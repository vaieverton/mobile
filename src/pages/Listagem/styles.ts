
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    filters: {
        marginBottom: 24,
        flexDirection: 'row'
    },
    red: {
        width: 16,
        height: 16,
        borderRadius: 8,
        backgroundColor: 'red'
    },
    green: {
        backgroundColor: 'green',
        width: 16,
        height: 16,
        borderRadius: 8,

    },
    yellow: {
        backgroundColor: 'yellow',
        width: 16,
        height: 16,
        borderRadius: 8,
        

    },
    ordenacao: {
        backgroundColor: '#999'
    },
    data: {
        marginTop: 8,
        backgroundColor: '#999'
    }
})

export default styles;